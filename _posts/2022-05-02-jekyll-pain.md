---
layout: posts
title: Bad whitespace in YAML triggers "mapping error" in Jekyll 
tag: jekyll
---

# YAML indentation woes

Following the instructions [here](https://jekyllrb.com/docs/step-by-step/06-data-files/) for setting up jekyll, got the following errors.

      Regenerating: 1 file(s) changed at 2022-05-02 13:51:38
                    _includes/navigation.html
             Error: (/Users/no-one/Documents/Code/blog/_data/navigation.yml): mapping values are not allowed in this context at line 2 column 9
             Error: Run jekyll build --trace for more information.

      Regenerating: 1 file(s) changed at 2022-05-02 13:51:52
                    _includes/navigation.html
             Error: (/Users/no-one/Documents/Code/blog/_data/navigation.yml): mapping values are not allowed in this context at line 2 column 9
             Error: Run jekyll build --trace for more information.

The issue was copying and pasting the content for `navigation.yml`.  It needs to be lined-up as in the instructions.

    - name: Home
      link: /
    - name: About
      link: /about.html

Any extra indentation that might "helpfully" get added will trigger this error.
