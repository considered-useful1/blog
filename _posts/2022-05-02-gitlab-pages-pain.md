---
layout: posts
title: gitlab pages blog links in jekyll
tag: jekyll gitlab
---

Having gone through the [jekyll guide](https://jekyllrb.com/docs/step-by-step/05-includes/) to add navigation, found that following any of the links would ask me to log in to gitlab.

Issue was that the navigation links pointed to the domain name rather than the domain name + repo name that gitlab hosts the pages on.  The following change to `_config.yml` mostly fixed the issue.

    baseurl: /blog

I also had to modify the `blog.html` from the [jekyll docs](https://jekyllrb.com/docs/step-by-step/08-blogging/#list-posts) to put the post url through the `absolute_url` filter.

{% raw %}
    <ul>
        {% for post in site.posts %}
        <li>
            <h2><a href="{{ post.url | absolute_url }}">{{ post.title }}</a></h2>
            {{ post.excerpt }}
            {% for item in post.tags %} {{item}} {% endfor %}
        </li>
        {% endfor %}
    </ul>
{% endraw %}

There's a lot more info [here](https://mademistakes.com/mastering-jekyll/site-url-baseurl/).
